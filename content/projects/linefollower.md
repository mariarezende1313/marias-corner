---
author: "Thunderleague team from Thunderatz"
title: "Thunderleague"
description: "RSS2D simulated team, which tries to copy real soccer players’ decisions. Implemented reinforcement
learning and neural networks in decision systems through data extracted from the games."
tags: ["thunderatz","nn","c++"]
thumbnail: https://mdbcdn.b-cdn.net/img/Photos/Slides/img%20(22).webp
---

RSS2D simulated team, which tries to copy real soccer players’ decisions. Implemented reinforcement
learning and neural networks in decision systems through data extracted from the games.

<!--more-->

## Competitions
- IRONCup 2021 - 1st place with ThunderLeague in RSS2D
- IranOpen 2021 - 11th place with ThunderLeague in RSS2D
- RoboCup 2021 - 14th place with ThunderLeague in RSS2D