---
author: "Thunderleague team from Thunderatz"
title: "Thunderleague"
description: "RSS2D simulated team, which tries to copy real soccer players’ decisions. Implemented reinforcement
learning and neural networks in decision systems through data extracted from the games."
tags: ["thunderatz","nn","c++"]
thumbnail: https://wallpaperaccess.com/full/269413.jpg
---

RSS2D simulated team, which tries to copy real soccer players’ decisions. Implemented reinforcement
learning and neural networks in decision systems through data extracted from the games.

<!--more-->

## Competitions
- IRONCup 2021 - 1st place with ThunderLeague in RSS2D
- IranOpen 2021 - 11th place with ThunderLeague in RSS2D
- RoboCup 2021 - 14th place with ThunderLeague in RSS2D