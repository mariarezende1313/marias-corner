$( document ).ready(function() {
    html.dataset.theme = 'theme-light';
    $("#light_button img").attr("src",'https://cdn-icons-png.flaticon.com/512/95/95148.png');
    $("#light_button img").css({"-webkit-filter":"invert(1)","filter": "invert(0)"});
    $('.carousel').carousel()

});


const html = document.querySelector('html');
function switchTheme() {
  if(html.dataset.theme == 'theme-light'){
        html.dataset.theme = 'theme-dark';
        $("#light_button img").attr("src",'https://cdn-icons-png.flaticon.com/512/95/95149.png');
        $("#light_button img").css({"-webkit-filter":"invert(1)","filter": "invert(1)"})
        }
    else{
        html.dataset.theme = 'theme-light';
        $("#light_button img").attr("src",'https://cdn-icons-png.flaticon.com/512/95/95148.png');
        $("#light_button img").css({"-webkit-filter":"invert(1)","filter": "invert(0)"})
    }
    
}

